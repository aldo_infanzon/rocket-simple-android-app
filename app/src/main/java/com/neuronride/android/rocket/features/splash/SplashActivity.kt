package com.neuronride.android.rocket.features.splash

import android.content.Intent
import android.os.Bundle
import com.neuronride.android.rocket.features.base.BaseActivity
import com.neuronride.android.rocket.features.store.StoreActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(
            Intent(this, StoreActivity::class.java)
        )

        finish()
    }
}