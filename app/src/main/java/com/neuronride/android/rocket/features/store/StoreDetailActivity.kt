package com.neuronride.android.rocket.features.store

import android.net.Uri
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.neuronride.android.rocket.COMPONENT
import com.neuronride.android.rocket.R
import com.neuronride.android.rocket.R.string
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.features.base.BaseActivity
import com.neuronride.android.rocket.features.store.view.model.StoreDetailViewModel
import com.neuronride.android.rocket.util.createStoreStaticMap
import com.neuronride.android.rocket.util.imageEvictandSet
import com.neuronride.android.rocket.util.randomHeaderImage
import com.neuronride.android.rocket.util.setImageCacheStrategy
import kotlinx.android.synthetic.main.activity_store_detail.addressTextView
import kotlinx.android.synthetic.main.activity_store_detail.cityStateZipTextView
import kotlinx.android.synthetic.main.activity_store_detail.logoImageView
import kotlinx.android.synthetic.main.activity_store_detail.mapImageView
import kotlinx.android.synthetic.main.activity_store_detail.phoneTextView
import kotlinx.android.synthetic.main.activity_store_detail.storeImageView
import kotlinx.android.synthetic.main.activity_store_detail.titleTextView
import kotlinx.android.synthetic.main.activity_store_detail.toolbar
import javax.inject.Inject

class StoreDetailActivity :
    BaseActivity() {

    companion object {
        const val STORE_ID_KEY = "STORE_ID_KEY"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: StoreDetailViewModel
    private var id: Long = -1L

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(STORE_ID_KEY, id)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_store_detail)

        COMPONENT.inject(this)

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )[StoreDetailViewModel::class.java]

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        id = savedInstanceState?.getLong(STORE_ID_KEY)
            ?: intent.getLongExtra(STORE_ID_KEY, -1L)

        if (id == -1L) {
            throw NullPointerException("Id must be supplied")
        }

        initStoreObservables()
    }

    private fun initStoreObservables() {

        viewModel.storeLiveData.observe(this, Observer<Store>
        { store ->
            val headerImageUri = Uri.parse(randomHeaderImage)
            val logoImageUri = Uri.parse(store.storeLogoUrl)
            val mapImageUri = Uri.parse(
                createStoreStaticMap(
                    store.latitude,
                    store.longitude
                )
            )
            val phoneNumber = PhoneNumberUtils.formatNumber(store.phone)
            val cityStateZip = getString(
                string.activity_store_detail_label,
                store.city,
                store.state,
                store.zipCode
            )

            supportActionBar?.title = store.name
            toolbar.title = store.name

            storeImageView.imageEvictandSet(headerImageUri)
            logoImageView.setImageCacheStrategy(logoImageUri)
            mapImageView.setImageCacheStrategy(mapImageUri)
            titleTextView.text = store.name
            phoneTextView.text = phoneNumber
            addressTextView.text = store.street
            cityStateZipTextView.text = cityStateZip
        })

        viewModel.storeFailureLiveData.observe(this, Observer {
            Toast.makeText(
                this,
                string.activity_store_detail_data_loading_error,
                Toast.LENGTH_LONG
            ).show()
        })

        viewModel.searchStoreById(id)
    }
}