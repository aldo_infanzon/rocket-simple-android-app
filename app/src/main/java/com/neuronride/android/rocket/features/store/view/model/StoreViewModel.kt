package com.neuronride.android.rocket.features.store.view.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.util.defaultSchedulers
import com.neuronride.rocket.framework.domain.StoreService
import io.reactivex.disposables.Disposables
import timber.log.Timber
import javax.inject.Inject

class StoreViewModel @Inject constructor(private val service: StoreService) : ViewModel() {
    val storeFailureLiveData = MutableLiveData<Throwable>()
    val storeLiveData = MutableLiveData<List<Store>>()

    private val storeList: MutableList<Store> = mutableListOf()

    private var storesDisposable = Disposables.disposed()

    override fun onCleared() {
        storesDisposable.dispose()
        super.onCleared()
    }

    fun getStoresList() {
        if (storeList.isNotEmpty()) {
            storeLiveData.postValue(storeList)
        } else {
            if (storesDisposable.isDisposed) {
                storesDisposable = service.fetchAllStores()
                    .onErrorResumeNext(service.getAllStores())
                    .defaultSchedulers()
                    .subscribe({
                        storeList.addAll(it)
                        storeLiveData.postValue(storeList)
                    }, { throwable ->
                        Timber.e(throwable)
                        storeFailureLiveData.postValue(throwable)
                    })
            }
        }
    }
}