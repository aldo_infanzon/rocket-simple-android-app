package com.neuronride.android.rocket.features.store

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.neuronride.android.rocket.COMPONENT
import com.neuronride.android.rocket.R
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.features.base.BaseActivity
import com.neuronride.android.rocket.features.base.BaseRecyclerAdapter
import com.neuronride.android.rocket.features.store.adapter.StoreRecyclerAdapter
import com.neuronride.android.rocket.features.store.view.model.StoreViewModel
import kotlinx.android.synthetic.main.activity_store.errorTextView
import kotlinx.android.synthetic.main.activity_store.progressBar
import kotlinx.android.synthetic.main.activity_store.recyclerView
import kotlinx.android.synthetic.main.activity_store.toolbar
import javax.inject.Inject

private const val RECYCLER_STATE_KEY = "RECYCLER_STATE_KEY"

class StoreActivity :
    BaseActivity(),
    BaseRecyclerAdapter.OnAdapterRowChanges<Store> {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var recyclerState: Parcelable? = null

    private lateinit var viewModel: StoreViewModel
    private lateinit var adapter: StoreRecyclerAdapter

    override fun onSaveInstanceState(outState: Bundle) {

        recyclerView.layoutManager?.let { state ->
            outState.putParcelable(RECYCLER_STATE_KEY, state.onSaveInstanceState())
        }

        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_store)

        COMPONENT.inject(this)

        savedInstanceState?.let { savedInstance ->
            if (savedInstance.containsKey(RECYCLER_STATE_KEY)) {
                recyclerState = savedInstanceState.getParcelable(RECYCLER_STATE_KEY)
            }
        }

        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )[StoreViewModel::class.java]

        initStoreObservables()
    }

    override fun onRowClick(view: View, position: Int, item: Store) {
        val intent = Intent(this, StoreDetailActivity::class.java)
        intent.putExtra(StoreDetailActivity.STORE_ID_KEY, item.id)
        startActivity(intent)
    }

    private fun initStoreObservables() {
        viewModel.storeLiveData.observe(this, Observer<List<Store>>
        { list ->
            if (!::adapter.isInitialized) {
                adapter = StoreRecyclerAdapter(
                    list.toMutableList(),
                    this
                )

                recyclerView.adapter = adapter

                // If there's a previous state  restore it
                recyclerState?.let { state ->
                    recyclerView.layoutManager?.onRestoreInstanceState(state)
                }
            } else {
                adapter.setItems(list)
                adapter.notifyDataSetChanged()
            }

            progressBar.visibility = View.GONE
        })
        viewModel.storeFailureLiveData.observe(this, Observer {
            progressBar.visibility = View.GONE
            errorTextView.visibility = View.VISIBLE
        })

        viewModel.getStoresList()
    }
}