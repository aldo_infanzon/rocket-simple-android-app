package com.neuronride.android.rocket.features.store.adapter

import android.view.ViewGroup
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.features.base.BaseRecyclerAdapter
import com.neuronride.android.rocket.features.base.BaseViewHolder

class StoreRecyclerAdapter(
    private val itemList: MutableList<Store>,
    clickListener: OnAdapterRowChanges<Store>?
) : BaseRecyclerAdapter<Store>(itemList, clickListener) {

    init {
        hasStableIds()
    }

    override fun getItemViewType(position: Int) = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            0 -> StoreRowViewHolder(parent)
            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is StoreRowViewHolder -> {
                holder.bind(
                    item = itemList[position],
                    callback = this
                )
            }
            else -> throw NotImplementedError()
        }
    }

    override fun onViewRecycled(holder: BaseViewHolder<*>) {
        // Always Clean any internal references from holder
        holder.clean()
        super.onViewRecycled(holder)
    }

    // To Support Stable Id's
    override fun getItemId(position: Int) = itemList[position].id

}