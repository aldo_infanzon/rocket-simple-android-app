package com.neuronride.android.rocket.di.module

import android.app.Application
import com.neuronride.rocket.framework.Framework
import com.neuronride.rocket.framework.RocketFramework
import com.neuronride.rocket.framework.domain.StoreService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FrameworkModule {

    @Singleton
    @Provides
    fun provideRocketFramework(
        application: Application
    ): Framework = RocketFramework.init(application)

    @Provides
    fun provideRocketStoreService(
        framework: Framework
    ): StoreService = framework.storeService
}