package com.neuronride.android.rocket.di.component

import com.neuronride.android.rocket.features.store.StoreActivity
import com.neuronride.android.rocket.features.store.StoreDetailActivity
import com.neuronride.android.rocket.di.module.ApplicationModule
import com.neuronride.android.rocket.di.module.FrameworkModule
import com.neuronride.android.rocket.di.module.ViewModelModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        FrameworkModule::class,
        ViewModelModule::class
    ]
)
interface ApplicationComponent {

    fun inject(target: StoreActivity)

    fun inject(target: StoreDetailActivity)
}