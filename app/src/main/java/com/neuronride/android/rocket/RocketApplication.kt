package com.neuronride.android.rocket

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.neuronride.android.rocket.di.component.ApplicationComponent
import com.neuronride.android.rocket.di.component.DaggerApplicationComponent
import com.neuronride.android.rocket.di.module.ApplicationModule
import timber.log.Timber
import timber.log.Timber.DebugTree

lateinit var COMPONENT: ApplicationComponent

class RocketApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        Fresco.initialize(this)

        COMPONENT = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}