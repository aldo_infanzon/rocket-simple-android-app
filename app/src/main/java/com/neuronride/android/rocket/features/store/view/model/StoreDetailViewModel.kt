package com.neuronride.android.rocket.features.store.view.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.util.defaultSchedulers
import com.neuronride.rocket.framework.domain.StoreService
import io.reactivex.disposables.Disposables
import timber.log.Timber
import javax.inject.Inject

class StoreDetailViewModel @Inject constructor(
    private val service: StoreService
) : ViewModel() {

    private var store: Store? = null
    val storeLiveData = MutableLiveData<Store>()
    val storeFailureLiveData = MutableLiveData<Throwable>()

    private var storeDisposable = Disposables.disposed()

    override fun onCleared() {
        storeDisposable.dispose()
        super.onCleared()
    }

    fun searchStoreById(id: Long) {
        store?.let {
            storeLiveData.postValue(it)
        } ?: run {
            if (storeDisposable.isDisposed) { // Only called when is disposed
                storeDisposable = service.getStoreById(id)
                    .defaultSchedulers()
                    .subscribe({
                        store = it
                        storeLiveData.postValue(it)
                    }, { throwable ->
                        Timber.e(throwable)
                        storeFailureLiveData.postValue(throwable)
                    })
            }
        }
    }
}