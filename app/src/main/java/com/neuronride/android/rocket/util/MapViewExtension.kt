package com.neuronride.android.rocket.util

import com.neuronride.android.rocket.BuildConfig

fun createStoreStaticMap(
    latitude: String?,
    longitude: String?
) = "https://maps.googleapis.com/maps/api/staticmap?" +
        "center=$latitude,$longitude" +
        "&size=1200x800" +
        "&maptype=roadmap" +
        "&zoom=17" +
        "&markers=color:red%7C$latitude,$longitude" +
        "&key=${BuildConfig.GOOGLE_MAPS_STATIC}"