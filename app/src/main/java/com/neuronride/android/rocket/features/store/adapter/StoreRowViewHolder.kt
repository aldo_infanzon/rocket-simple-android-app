package com.neuronride.android.rocket.features.store.adapter

import android.net.Uri
import android.view.ViewGroup
import com.jakewharton.rxbinding3.view.clicks
import com.neuronride.android.rocket.R
import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.android.rocket.features.base.BaseViewHolder
import com.neuronride.android.rocket.util.CLICK_DELAY_MILLISECONDS
import com.neuronride.android.rocket.util.setImageCacheStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.view_row_store_item.view.*
import java.util.concurrent.TimeUnit

class StoreRowViewHolder(
    parent: ViewGroup
) : BaseViewHolder<Store>(
    parent,
    R.layout.view_row_store_item
) {

    private var rowClickDisposable = Disposables.disposed()

    override lateinit var item: Store

    override fun clean() {
        rowClickDisposable.dispose()
    }

    override fun bind(item: Store, callback: RowCallback<Store>) {
        this.item = item

        itemView.storeImageView.setImageCacheStrategy(Uri.parse(item.storeLogoUrl))
        itemView.titleTextView.text = item.name

        rowClickDisposable = itemView.clicks()
            .throttleFirst(CLICK_DELAY_MILLISECONDS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                callback.rowClick(itemView, adapterPosition, this.item)
            }
    }

}