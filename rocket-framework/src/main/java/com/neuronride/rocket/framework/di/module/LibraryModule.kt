package com.neuronride.rocket.framework.di.module

import android.app.Application
import android.content.Context
import com.neuronride.rocket.framework.R
import com.neuronride.rocket.framework.data.datasource.store.StoreApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val TIMEOUT_MILLIS = 15000L
private const val READ_TIMEOUT_MILLIS = 15000L
private const val WRITE_TIMEOUT_MILLIS = 15000L
private const val CALL_TIMEOUT_MILLIS = 30000L

@Module
class LibraryModule(private val application: Application) {

    @Provides
    fun provideContext(): Context = application

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
            .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .callTimeout(CALL_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
            .build()

    @Singleton
    @Provides
    fun prividesRocketStoreApiRetrofit(client: OkHttpClient): StoreApi =
        Retrofit.Builder()
            .baseUrl(application.getString(R.string.rocket_base_api_host))
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(StoreApi::class.java)
}