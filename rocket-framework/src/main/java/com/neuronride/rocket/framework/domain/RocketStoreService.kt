package com.neuronride.rocket.framework.domain

import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.rocket.framework.data.gateway.store.StoreGateway
import io.reactivex.Completable
import io.reactivex.Single

class RocketStoreService(
    private val gateway: StoreGateway
) : StoreService {

    override fun fetchAllStores(): Single<List<Store>> =
        gateway.fetchAll()
            .flatMapCompletable {
                storeAll(it)
            }.andThen(getAllStores())

    override fun getAllStores(): Single<List<Store>> =
        gateway.getAll()

    override fun getStoreById(id: Long): Single<Store> =
        gateway.getStoreById(id)

    override fun storeAll(list: List<Store>): Completable =
        gateway.storeAll(list)
}