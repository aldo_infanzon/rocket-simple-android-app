package com.neuronride.rocket.framework.domain

import com.neuronride.rocket.framework.data.model.Store
import io.reactivex.Completable
import io.reactivex.Single

interface StoreService {

    fun fetchAllStores(): Single<List<Store>>

    fun getStoreById(id: Long): Single<Store>

    fun getAllStores(): Single<List<Store>>

    fun storeAll(list: List<Store>): Completable
}