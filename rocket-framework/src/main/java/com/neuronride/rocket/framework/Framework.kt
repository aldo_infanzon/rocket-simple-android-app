package com.neuronride.rocket.framework

import com.neuronride.rocket.framework.domain.StoreService

interface Framework {
    var storeService: StoreService
}