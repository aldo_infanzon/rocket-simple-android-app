package com.neuronride.rocket.framework.data.datasource.store

import com.neuronride.rocket.framework.data.model.Store
import io.reactivex.Flowable
import retrofit2.http.GET

interface StoreApi {

    @GET("stores.json")
    fun fetchAll(): Flowable<StoreResponse>

    data class StoreResponse(
        var stores: List<Store>
    )
}