package com.neuronride.rocket.framework.di.module

import com.neuronride.rocket.framework.data.datasource.store.StoreApi
import com.neuronride.rocket.framework.data.datasource.store.StoreDao
import com.neuronride.rocket.framework.data.gateway.store.RocketStoreGateway
import com.neuronride.rocket.framework.data.gateway.store.StoreGateway
import dagger.Module
import dagger.Provides

@Module
class GatewayModule {

    @Provides
    fun provideRocketStoreGateway(
        api: StoreApi,
        dao: StoreDao
    ): StoreGateway = RocketStoreGateway(dao, api)
}