package com.neuronride.rocket.framework.di.module

import android.content.Context
import com.neuronride.rocket.framework.data.datasource.DefaultDatabase
import com.neuronride.rocket.framework.data.datasource.store.StoreDao
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun providesDatabase(
        context: Context
    ): DefaultDatabase = DefaultDatabase.getInstance(context)

    @Provides
    fun provideStoreDao(
        database: DefaultDatabase
    ): StoreDao = database.storeDao()
}