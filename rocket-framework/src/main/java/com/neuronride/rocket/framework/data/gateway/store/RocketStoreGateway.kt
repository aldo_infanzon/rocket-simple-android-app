package com.neuronride.rocket.framework.data.gateway.store

import com.neuronride.rocket.framework.data.model.Store
import com.neuronride.rocket.framework.data.datasource.store.StoreApi
import com.neuronride.rocket.framework.data.datasource.store.StoreDao
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class RocketStoreGateway(
    private val dao: StoreDao,
    private val api: StoreApi
) : StoreGateway {

    override fun fetchAll(): Flowable<List<Store>> =
        api.fetchAll()
            .map {response ->
                response.stores
            }

    override fun getAll(): Single<List<Store>> =
        dao.getAll()

    override fun getStoreById(id: Long): Single<Store> =
        dao.findById(id)

    override fun storeAll(list: List<Store>): Completable =
        dao.insert(*list.toTypedArray())
}