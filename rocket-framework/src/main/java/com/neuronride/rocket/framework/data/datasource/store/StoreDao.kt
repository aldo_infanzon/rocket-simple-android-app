package com.neuronride.rocket.framework.data.datasource.store

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.neuronride.rocket.framework.data.model.Store
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class StoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: Store): Single<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vararg item: Store): Completable

    @Query("SELECT * from store_table ORDER BY name ASC")
    abstract fun getAll(): Single<List<Store>>

    @Query("SELECT * from store_table WHERE id = :id")
    abstract fun findById(id: Long): Single<Store>

    @Query("DELETE FROM store_table")
    abstract fun deleteAll(): Completable
}