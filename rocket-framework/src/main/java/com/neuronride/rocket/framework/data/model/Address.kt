package com.neuronride.rocket.framework.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

open class Address {

    @SerializedName("address")
    var street: String? = null

    var city: String? = null

    var state: String? = null

    @SerializedName("zipcode")
    @ColumnInfo(name = "zip_code")
    var zipCode: String? = null

    var latitude: String? = null

    var longitude: String? = null

}