package com.neuronride.rocket.framework

import android.app.Application
import com.neuronride.rocket.framework.di.component.DaggerLibraryComponent
import com.neuronride.rocket.framework.di.component.LibraryComponent
import com.neuronride.rocket.framework.di.module.LibraryModule
import com.neuronride.rocket.framework.domain.StoreService
import javax.inject.Inject

private lateinit var COMPONENT: LibraryComponent

class RocketFramework private constructor() : Framework {

    companion object {
        fun init(application: Application): RocketFramework {
            val instance = RocketFramework()

            COMPONENT = DaggerLibraryComponent.builder()
                .libraryModule(LibraryModule(application))
                .build()

            COMPONENT.inject(instance)

            return instance
        }
    }

    @Inject
    override lateinit var storeService: StoreService
}