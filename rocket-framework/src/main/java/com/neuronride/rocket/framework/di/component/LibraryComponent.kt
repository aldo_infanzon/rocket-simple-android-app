package com.neuronride.rocket.framework.di.component

import com.neuronride.rocket.framework.RocketFramework
import com.neuronride.rocket.framework.di.module.GatewayModule
import com.neuronride.rocket.framework.di.module.LibraryModule
import com.neuronride.rocket.framework.di.module.RepositoryModule
import com.neuronride.rocket.framework.di.module.ServiceModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        LibraryModule::class,
        ServiceModule::class,
        RepositoryModule::class,
        GatewayModule::class
    ]
)
interface LibraryComponent {

    fun inject(target: RocketFramework)
}