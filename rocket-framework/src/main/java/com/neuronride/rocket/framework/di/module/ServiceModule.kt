package com.neuronride.rocket.framework.di.module

import com.neuronride.rocket.framework.data.gateway.store.StoreGateway
import com.neuronride.rocket.framework.domain.RocketStoreService
import com.neuronride.rocket.framework.domain.StoreService
import dagger.Module
import dagger.Provides

@Module
class ServiceModule {

    @Provides
    fun provideRocketStoreService(
        gateway: StoreGateway
    ): StoreService = RocketStoreService(gateway)
}