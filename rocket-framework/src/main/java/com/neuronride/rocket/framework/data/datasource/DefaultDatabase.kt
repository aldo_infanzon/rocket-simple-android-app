package com.neuronride.rocket.framework.data.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.neuronride.rocket.framework.data.datasource.store.StoreDao
import com.neuronride.rocket.framework.data.model.Store

private const val VERSION = 1
private const val NAME = "rocket-default-database"

@Database(
    entities = [
        Store::class
    ],
    version = VERSION
)
abstract class DefaultDatabase : RoomDatabase() {

    companion object {

        @Volatile
        private var INSTANCE: DefaultDatabase? = null

        fun getInstance(context: Context): DefaultDatabase =
            INSTANCE
                ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(
                        context
                    ).also { db ->
                    INSTANCE = db
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DefaultDatabase::class.java,
                NAME
            ).build()
    }

    abstract fun storeDao(): StoreDao

}
