package com.neuronride.rocket.framework.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

const val TABLE_NAME = "store_table"

@Entity(tableName = TABLE_NAME)
data class Store(

    @PrimaryKey
    @SerializedName("storeID")
    @ColumnInfo(name = "id")
    var id: Long,

    @SerializedName("storeLogoURL")
    @ColumnInfo(name = "store_logo_url")
    var storeLogoUrl: String?,

    var name: String?,

    var phone: String?

) : Address()